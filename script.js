// Viết chương trình có một ô input, một button. 
// Khi click vào button thì in ra các số nguyên tố từ 1 tới giá trị của ô input
// input: 1 số 
// action:
// viết 1 hàm có tham số n dùng để xác định số n có phải là số nguyên tố hay ko, nếu phải thì trả về true, ngược lại 
// sau đó viết 1 hàm lấy giá trị người dùng nhập vào, khai báo 1 mảng rỗng, sau đó cho 1 vòng lập run từ 2->giá trị người
// dùng nhập, trong vòng lập ta ứng dụng kết quả của hàm xác định số nguyên tố hay ko, nếu phải ta push i vào mảng rỗng,
// cuối cùng xuất ra kết quả
// uotput: các số nguyên tố từ 2-giá trị người dụng nhập

function isSonguyento(n){
    let isNguyento=true 
    for(let i=2; i<n;i++){
        if(n%i==0){
            isNguyento=false
        }
    }
    if(isNguyento){
        return true
    }else{
        return false
    }
}

function handleClick(){
    const value=document.getElementById("value").value*1
    const result=document.getElementById("result")
    const arrA=[]
    if(value>1){
        for(let i=2; i<=value;i++){
            if(isSonguyento(i)){
                arrA.push(i)
            }
        }
        result.innerHTML=arrA
    }else {
        return 
    }
    
}